# Tests

All the tests have been written using `unittest` module. Please launch all of them from a command line interface as

```bash
python3 -m unittest
```

and contact the authors (by issue raising on https://framagit.org/nlp/iamtokenizing/) in case there are some failing test.