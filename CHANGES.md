# Changes along the versions

Each main version comes with its own documentation, that is available as a [Jupyter-book](https://jupyterbook.org) that you can consult by opening `documentation/_build/html/index.html` on a web-browser. To generate the documentation, you need to extract the tagged versions on the [official repository](https://framagit.org/nlp/tokenspan), and to construct the corresponding documentation using `jupyter-book build documentation/`

## 0.7+

These versions are based on `ExtractionString` and `EvenSizedSortedSet`

### 0.7.0

## 0.6+

Versions 0.6+ are based on version 0.7.1 of [pypi:tokenspan](https://pypi.org/project/tokenspan/).

### 0.6.4

 - Add the `copy()` method to `RegexDetector` and `NGrams` (in fact it is done at the level of `BaseTokenizer`)
 - Correct the dependency, requirements.txt now mention extractionstring>=0.7.1

### 0.6.2
 - Correct the dependency, requirements.txt now mention tokenspan>=0.1.0

*This version has been uploaded on [pypi:iamtokenizing/0.6.2](https://pypi.org/project/iamtokenizing/0.6.2/) and has [tag v0.6.2](https://framagit.org/nlp/iamtokenizing/-/tags/v0.6.2) on the official repository*

### 0.6.1
 - It is now possible to convert NGrams to RegexDetector, resolving issue #5
 - The attributes `chargrams`, `ngrams` and `detections` in classes `CharGrams`, `NGrams` and `RegexDetector` have all been drop, the only attribute is now `tokens`, that is filled (as a list of the class instances) by the `tokenize` method of each class.
 - The attribute `context_tokens` in `ContextDetector` has been replaced by `tokens`
 - BaseTokenizer inherits from Span class (from [tokenspan](https://framagit.org/nlp/tokenspan/) package) and the child classes no more inherit directly from Span, they inherit only from BaseTokenizer
 - NGrams has no more attribute `regex`


## 0.5 + 

### 0.5.6
 - BaseTokenizer can be imported by a `from iamtokenizing import BaseTokenizer` in order to make typing more effective
 - BaseTokenizer no more has a `__repr__` magic function, since the children tokenizers also inherit from Span
 - BaseTokenizer has the attribute (in the form of a property-decorator) `_subtokens_` raising a `NotImplementedError` to make a BaseTokenizer a usual interface
 - BaseTokenzier has the `NotImplementedError`-raising method `tokenize` in order to make this class a usual interface
 - CharGrams, NGrams, RegexDetector and ContextDetector no more have a `_name_` attribute, that was useless (without warning since users were in principle not abble to parse this attribute)
 - ContextDetector has been debugged, see issue #4: ContextDetector was returning the entire string in case there was no detection at all in the document
 
*This version has been uploaded on [pypi:iamtokenizing/0.5.6/](https://pypi.org/project/iamtokenizing/0.5.6/) and has [tag v0.5.6](https://framagit.org/nlp/iamtokenizing/-/tags/v0.5.6) on the official repository*

### 0.5.5
 - ContextDetector class appears
 - constructor has now a ranges=None value by default. This allows to construct more relevant combinations of ranges afterwards. This is due to a change in the Span object.
 - methods `toSpans`, `toStrings`, `toTokens` have been aliased by `to_spans`, `to_strings`, `to_tokens`, and there is no more mention of the JAVA style naming in the tutorials.
 - depends on `tokenspan` version 0.6.0, requirements updated.

*This version has been uploaded on [pypi:iamtokenizing/0.5.5/](https://pypi.org/project/iamtokenizing/0.5.5/) and has [tag v0.5.5](https://framagit.org/nlp/iamtokenizing/-/tags/v0.5.5) on the official repository*

### 0.5.2
 - add the possibility to tune `inplace` option of the `CharGrams`, `NGrams` and `RegexDetector` tokenizers
 - the tokenizers are now hashable, such that one can do a set(list of Tokenizer) to withdraw similar elements (having same parent string and same ranges)




