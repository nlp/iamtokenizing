# Documentation of the package `iamtokenizing`

The documentation is available on [https://nlp.frama.io/iamtokenizing/](https://nlp.frama.io/iamtokenizing/)

The PyPi package is available on [https://pypi.org/project/iamtokenizing/](https://pypi.org/project/iamtokenizing/)

The official repository is on [https://framagit.org/nlp/iamtokenizing](https://framagit.org/nlp/iamtokenizing)

## `CharGrams`, `NGrams`, `RegexDetector` classes

- `CharGrams` cuts a string into contiguous and overlapping ranges of characters
- `NGrams` cuts a string based on some REGular EXpression (REGEX). The case by default is to cut the initial string on its white space for instance
- `RegexDetector` extracts the tokens that correspond to a REGular EXpression (REGEX).
- `ContextDetector` cut a text using `NGrams` that will serve as context, then detect inside that cuts using `RegexDetector`, and associate the context to the detected token

The tokenization takes place while calling the `tokenize()` method of these classes.

Note that `NGrams` and `RegexDetector` treats differently their associated REGEX: while `NGrams` will *discard* the matched patterns, `RegexDetector` will *keep* the matched patterns. 

All these classes are based on the `Span` object of the package [tokenspan](https://nlp.frama.io/tokenspan/). A `Span` is just a collection of ranges that select sub-part of a parent string. Thus the tokens constructed by the classes here will all have the same parent string (usually this is the text one wants to tokenize) and a collection of ranges (usually there is only one range since the REGEX returns contiguous matches) that correspond to the position of the characters inside the parent string. For instance `ranges=[range(start,stop)]` would correspond to `string[start:stop]`. In short, what a tokenizer does is to construct the collection of ranges, not to change the parent string (the change is a split when tokenization is in concern). The procedure of selecting `string[start:stop]` is encapsulated in several methods : 
 
 - `to_spans`, which returns a list of `Span` objects (described in the package [tokenspan](https://nlp.frama.io/tokenspan/))
 - `to_tokens`, which returns a list of `Token` objects (described in the package [tokenspan](https://nlp.frama.io/tokenspan/))
 - `to_strings`, which returns a tuple of strings

## Comparison with other libraries

What many standard libraries (nltk, gensim, scikit-learn, ...) call a *token* is one element of the list of the outcome of the `to_strings()` method. Here we give opportunities to the user to use simple algebra of the `Span` object, or to add attributes in a versatile way (using `Token` objects), see [tokenspan](https://nlp.frama.io/tokenspan/) documentation.
