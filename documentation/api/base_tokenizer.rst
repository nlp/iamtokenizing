BaseTokenizer class
===================

.. autoclass:: iamtokenizing.base_tokenizer.BaseTokenizer
   :members:
   :undoc-members:
   :special-members: __len__, _hash__, __contains__, __bool__, __getitem__, __eq__, __add__, __sub__, __mul__, __truediv__, __lt__, __gt__, __le__, __ge__, __init__,
   :show-inheritance: