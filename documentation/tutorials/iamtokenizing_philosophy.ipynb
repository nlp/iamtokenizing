{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b1877961",
   "metadata": {},
   "source": [
    "# Paradigms behind iamtokenizing\n",
    "\n",
    "We explore the main concepts of the library. This might serve as a quick introduction to the concepts of `Span` and tokenizers in the way they are implemented here."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b538e8b0",
   "metadata": {},
   "source": [
    "## `Span` originality\n",
    "\n",
    "Usually, a [tokenization](https://en.wikipedia.org/wiki/Lexical_analysis#Tokenization) consists in both the cleaning and splitting of a parent string, here called a _text_, into a collection of short strings, that one may see as atomic entities representing the parent text, and here callde _words_. Once these words are obtained, one can give them to subsequent treatment processes of natural language processing (NLP). \n",
    "\n",
    "We illustrate a simple tokenization usual procedure using the string methods of the standard library. The text is `'Simple string for demonstration and for illustration.'`, as simple as possible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "ed788c45",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Simple', 'string', 'for', 'demonstration', 'and', 'for', 'illustration.']"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "text = 'Simple string for demonstration and for illustration.'\n",
    "tokens = text.split()\n",
    "tokens"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e09bf828",
   "metadata": {},
   "source": [
    "Though being not perfect (there are still uppercase characters, and punctuations is kept), this tokenization is sufficient for our illustration. \n",
    "\n",
    "One sees that we completely lost the notion of text in this collection of words. In contrary, in the `iamtokenizing` library, one wants to construct a faithfull representation of the text in terms of words. The solution proposed by the library is to represent the tokenization as the construction of intervals (`range`s objects in Python) on top of a text. \n",
    "\n",
    "Indeed, a text is a sequence of characters, and each characters has a unique position in this sequence. To construct a faithfull representation of words, we attribute a range to each of these words, as illustrated below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "691ed8a9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('Simple string for demonstration and for illustration.', [(0,6),(7,13),(14,17),(18,31),(32,35),(36,39),(40,53)])"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from iamtokenizing import NGrams, RegexDetector\n",
    "\n",
    "iamtokens = NGrams(text)\n",
    "iamtokens.tokenize()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74bfdd4d",
   "metadata": {},
   "source": [
    "This might not look like a collection of tokens, because the tokens themselves are hidden in the `.tokens` list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "171becb8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[NGrams('Simple', [(0,6)]),\n",
       " NGrams('string', [(7,13)]),\n",
       " NGrams('for', [(14,17)]),\n",
       " NGrams('demonstration', [(18,31)]),\n",
       " NGrams('and', [(32,35)]),\n",
       " NGrams('for', [(36,39)]),\n",
       " NGrams('illustration.', [(40,53)])]"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "iamtokens.tokens"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ee49b5c",
   "metadata": {},
   "source": [
    "Each object in `NGrams.tokens` is an `NGrams` instance (this is the same for the other tokenizers `CharGrams`, `RegexDetector`, ...). They are all child-class of the `Span` class developped in the package [tokenspan](https://pypi.org/project/tokenspan/). \n",
    "\n",
    "At its basic level, a `Span` object is the collection of \n",
    " - a parent string, the _text_\n",
    " - a collection of `range`s that select the sequences of characters in the parent string\n",
    " - a character that re-glue the sub-sequences of characters when they are non-contiguous\n",
    "\n",
    "The only restriction on `Span` is that the `range`s should never overlap (in fact the constructor of the class combine the overlapping `range`s in case they are some).\n",
    "\n",
    "Since one discuss the tokenization as a collection of `range`s objects on top of a parent string instead of a collection of string directly, one has mathematical operations allowed to the `Span` obejct, and inherited to all its child-classes in the `iamtokenizing` package."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9289f784",
   "metadata": {},
   "source": [
    "## Ordering of tokens\n",
    "\n",
    "The only ordering operation allowed in string is the [lexicographic order](https://en.wikipedia.org/wiki/Lexicographic_order), that just corresponds to the alphabetical order of string, from the first character to the last one. \n",
    "\n",
    "In contrary, when seen as a collection of `range`s, a collection of tokens can be ordered using any kind of ordering of intervals. In the `Span` class, we adopt the reading order (of latin languages: from left to right). For instance each token above do not overlap with its right-most neighbor : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "d8259ccd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "True\n",
      "True\n",
      "True\n",
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "for token1, token2 in zip(iamtokens[:-1], iamtokens[1:]):\n",
    "    print(token1 < token2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bdc2a6c",
   "metadata": {},
   "source": [
    "In addition, they are not overlapping: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "8bda9fbb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n",
      "False\n",
      "False\n",
      "False\n",
      "False\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "for token1, token2 in zip(iamtokens[:-1], iamtokens[1:]):\n",
    "    print(token1 <= token2 or token1 >= token2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9ad15db0",
   "metadata": {},
   "source": [
    "The above example is quite basic and do not touch upon all the possibilities of the `tokenspan` package. \n",
    "\n",
    "**To go further:** One can imagine having several different collections of tokens, that are overlapping, and elaborate quite complicated ordering that would construct a meaningfull graph of ordered tokens. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bac6e8a2",
   "metadata": {},
   "source": [
    "## Algebraic manipulation of strings\n",
    "\n",
    "Seen as a collection of `range`s, a token is represented as a set of unique positions inside the parent string. As such, all the set operations are allowed from the `Span` object, and these operations are inherited at the tokenizer level.\n",
    "\n",
    "For instance, to construct bi-grams can be performed using the union operation (`+`) between consecutive tokens"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "aea213bc",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[NGrams('Simple string', [(0,6),(7,13)]),\n",
       " NGrams('string for', [(7,13),(14,17)]),\n",
       " NGrams('for demonstration', [(14,17),(18,31)]),\n",
       " NGrams('demonstration and', [(18,31),(32,35)]),\n",
       " NGrams('and for', [(32,35),(36,39)]),\n",
       " NGrams('for illustration.', [(36,39),(40,53)])]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bigrams = [tok1+tok2 for tok1, tok2 in zip(iamtokens[:-1], iamtokens[1:])]\n",
    "bigrams"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9268815d",
   "metadata": {},
   "source": [
    "And from them, one can illustrate the other operations : \n",
    " - `+` represents the union of tokens: all the sub-strings of both tokens (this operation is symmetric)\n",
    " - `*` represents the intersection of tokens: sub-strings in common among different tokens (this operation is symmetri), this represents also the overlapping parts of the two tokens\n",
    " - `-` represents the difference of tokens, in the form `token1 - token2`: sub-strings in `token1` that are not present in `token2` (this operation is not-symmetric), this operation also withdraw the overlapping part of the two tokens in `token1`\n",
    " - `/` represents the symmetric difference of tokens: the union of sub-strings when the intersection is withdrawn (this operation is symmetric, contrary to the usual representation of the division symbol for numbers), this operation withdraw the overlapping part of the two tokens ; note that `token1/token2 = (token1+token2)-(token1*token2)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "b60a478c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('string', [(7,13)])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bigrams[0] * bigrams[1] # intersection of tokens"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "aee58bed",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('Simple', [(0,6)])"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bigrams[0] - bigrams[1] # difference of tokens"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "11f9dde3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('for', [(14,17)])"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bigrams[1] - bigrams[0] # difference of tokens is not symmetric"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "12d32e08",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('Simple for', [(0,6),(14,17)])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bigrams[0] / bigrams[1] # symmetric difference of tokens"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "d89752d6",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('Simple for', [(0,6),(14,17)])"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bigrams[1] / bigrams[0] # symmetric difference of tokens is symmetric"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c910d891",
   "metadata": {},
   "source": [
    "One more time, the above example is quite uninteresting.\n",
    "\n",
    "**To go further:** algebraic manipulation of `Span` is particularly powerfull when one tries to extract information from a text. Once an information is extracted, the associated sub-string can be easilly withdrawn from the text by the difference operation for instance. Then more subtle information might be extracted from the remaining of the text, or looked after inside the previous token. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "b483ed65",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Last modification Fri Feb 25 07:39:56 2022\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "print(\"Last modification {}\".format(datetime.now().strftime(\"%c\")))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
