#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 21 15:01:48 2021

@author: pi
"""

from re import IGNORECASE
import urllib.request
from urllib.parse import quote, unquote
from unidecode import unidecode
from iamtokenizing import NGrams, RegexDetector

url = "https://en.wikipedia.org/wiki/Python_(programming_language)"
opener = urllib.request.urlopen(url)
f = opener.open(url)
content = opener.read()

regdet = RegexDetector(content)
anchors = regdet.tokenize(regex=r"<.*?>", inplace=False)

paragraph = regdet.tokenize(regex=r"<p(.|\n)*?</p>", flags=IGNORECASE, inplace=False)
paragraphs = list()
for parag in paragraph:
    paragraph_text = NGrams(string=content, ranges=parag.ranges)
    paragraph_text.tokenize(regex=r"<.*?>")
    paragraphs.append(str(paragraph_text))
len(paragraphs)

unquote(str(paragraph_text))

# generate some label with the balise
labels = set()
for anchor in anchors:
    tokens = anchor.tokenize(inplace=False)
    tokenstring = tokens.toStrings()
    label = '_'.join(unidecode(t.lower()) for t in tokenstring)
    labels.update((label,))


len(set(labels))

startwith = set(l.split('_')[0] for l in labels)
len(startwith)

not_starting_with = ['a_', 'li', 'td', 'ul', 'th', 'sup', 'style']
labels_filtered = set(l for l in labels 
                      if not any(l.startswith(slug) for slug in not_starting_with))
len(labels_filtered)

anchors[0]
anchors[-1]
inter_texts = regdet - anchors
inter_texts[0]
inter_texts[-1]
inter_texts.subSpans[-3]
