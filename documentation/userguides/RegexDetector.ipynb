{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# RegexDetector\n",
    "\n",
    "`RegexDetector` cuts a string using a REGular Expression (REGEX) to keep some patterns in the generated tokens.\n",
    "\n",
    "`RegexDetector` has all the parameters and attributes of a `Span` object (from [tokenspan](https://nlp.frama.io/tokenspan/) package), namely \n",
    " \n",
    " - a `string` that will store the complete text that one wants to tokenize\n",
    " - a list of `intervals`, that selects the sub-string of `string` onto which the tokenization will apply\n",
    " - a `subtoksep` character (preferably of length 1) that glues the sub-tokens (each sub-token being given by a given range in `ranges`) together. By default this is the white space character.\n",
    "\n",
    "Most of the time, one just needs to feed the `string` parameter with the text one wants to tokenize."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## RegexDetector.tokenize()\n",
    "\n",
    "The main method of all the tokenizers is `tokenize`. For `RegexDetector` this method has three parameters : \n",
    "\n",
    " - `regex` (default is `'\\w+'`) is the REGEX that characterizes the patterns that will be kept. By default the conserved tokens are any contiguous chain of characters.\n",
    " - `flags` (default is `0`) any flag that are compatible with the [re module](https://docs.python.org/3/library/re.html) from Python Standard Library. To construct the flag, one needs to import the regex module.\n",
    " - `inplace` (default is `True`), a boolean that precise whether the tokenization applies to the current instance, or generate a new instance of the tokenization class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Quite\n",
      "a\n",
      "Long\n",
      "TEXT\n",
      "with\n",
      "tabulations\n",
      "and\n",
      "new\n",
      "line\n",
      "FoR\n",
      "testing\n",
      "text\n",
      "tokenization\n"
     ]
    }
   ],
   "source": [
    "from iamtokenizing import RegexDetector\n",
    "\n",
    "string = \"Quite a Long TEXT with tabulations\\t and new line\\n\"\n",
    "string += \"FoR testing text tokenization.\"\n",
    "\n",
    "regdet = RegexDetector(string=string)\n",
    "regdet.tokenize()\n",
    "\n",
    "for token in regdet:\n",
    "    print(token)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's it, nothing more complicated. Note this is *not* exactly what the `NGrams` tokenizer would produce by default, since the last character of `string` is discarded here (indeed, a punctuation symbol is not an alpha-numeric one for REGEX). \n",
    "\n",
    "## Under the hood\n",
    "\n",
    "Under the hood, the tokenizer fills a list of token, each token being a sub-class of `Span`. Since the `Span` is just a collection of ranges on top of a parent string, each range representing a sub-string of the `Span.string`.\n",
    "\n",
    "For `RegexDetector` the sub-class of `Span` are `RegexDetector` objects themselves, and they are stored in the list `RegexDetector.tokens` (this is also the case for the other tokenizer in this package, though the name of the attribute varies)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[RegexDetector('Quite', [(0,5)]),\n",
       " RegexDetector('a', [(6,7)]),\n",
       " RegexDetector('Long', [(8,12)]),\n",
       " RegexDetector('TEXT', [(13,17)]),\n",
       " RegexDetector('with', [(18,22)]),\n",
       " RegexDetector('tabulations', [(23,34)]),\n",
       " RegexDetector('and', [(36,39)]),\n",
       " RegexDetector('new', [(40,43)]),\n",
       " RegexDetector('line', [(44,48)]),\n",
       " RegexDetector('FoR', [(49,52)]),\n",
       " RegexDetector('testing', [(53,60)]),\n",
       " RegexDetector('text', [(61,65)]),\n",
       " RegexDetector('tokenization', [(66,78)])]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "regdet.tokens"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Change the REGEX\n",
    "\n",
    "By default, the REGEX selects contiguous chain of alpha-numerics characters, and split when a non-alpha-numeric symbol appears (as e.g. punctuation, spaces, ...)\n",
    "\n",
    "One can use `RegexDetector` to detect terms inside a text and collect them. For instance below we take all words that starts by an uppercase character."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Quite\n",
      "Long\n",
      "TEXT\n",
      "FoR\n"
     ]
    }
   ],
   "source": [
    "detections = RegexDetector(string=string)\n",
    "detections.tokenize(regex='[A-Z]\\w+')\n",
    "for detection in detections: \n",
    "    print(detection)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inplace tokenization\n",
    "\n",
    "By default, one applies the tokenization inplace, that is, we change the actual instance of the object. This allows subtle treatment of the text, as e.g. finding the tokens that starts by an uppercase letter and contains a `'g'` character : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "RegexDetector('g', [(11,12)])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "detections.tokenize(regex='g')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(note it is obvioulsy possible to do that in one REGEX, here we use this two-steps tokenization for illustration only). \n",
    "\n",
    "In case one wants to change this inplace behavior, simply call the `inplace=False` parameter during tokenization. Below we redo the same two-step tokenization, using this option "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Quite\n",
      "Long\n",
      "TEXT\n",
      "FoR\n"
     ]
    }
   ],
   "source": [
    "detections = RegexDetector(string=string)\n",
    "uppercases = detections.tokenize(regex='[A-Z]\\w+', inplace=False)\n",
    "for detection in uppercases: \n",
    "    print(detection)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "RegexDetector('g g', [(11,12),(59,60)])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "detections.tokenize(regex='g')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This does not give us the same result as applying the `'g'` detection only on uppercase words. Of course, one can still call the tokenization on the captured first tokenization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "RegexDetector('g', [(11,12)])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "uppercases.tokenize(regex='g')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## REGEX flags\n",
    "\n",
    "Let us illustrate the flags. We have to import the `re` module to build the flags. We keep the previous REGEX, but we add the flag that allows to ignore the case of the characters. Then we recover the tokens obtained by default."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Quite\n",
      "Long\n",
      "TEXT\n",
      "with\n",
      "tabulations\n",
      "and\n",
      "new\n",
      "line\n",
      "FoR\n",
      "testing\n",
      "text\n",
      "tokenization\n"
     ]
    }
   ],
   "source": [
    "import re\n",
    "\n",
    "detections = RegexDetector(string=string)\n",
    "detections.tokenize(regex='[A-Z]\\w+', flags=re.IGNORECASE)\n",
    "for detection in detections: \n",
    "    print(detection)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transformation to `Span`, `Token` or `string`\n",
    "\n",
    "One can transform the tokens in `Span`, `Token` or string objects easilly from a `NGrams` instance. This allows easy manipulation of the token in further NLP treatments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "spans = regdet.to_spans()\n",
    "tokens = regdet.to_tokens()\n",
    "strings = regdet.to_strings()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Span('tokenization', [(66,78)])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "spans[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Token('tokenization', [(66,78)])"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tokens[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'tokenization'"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "strings[-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## RegexDetector to NGrams and vice-versa\n",
    "\n",
    "One can give the tokens to the kind-of opposite-in-task tokenizer quite easilly. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('Quite Long TEXT with tabulations and new line FoR testing text tokenization', [(0,5),(8,12),(13,17),(18,22),(23,34),(36,39),(40,43),(44,48),(49,52),(53,60),(61,65),(66,78)])"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from iamtokenizing import NGrams\n",
    "\n",
    "ngrams = NGrams(detections)\n",
    "ngrams"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This allows to make more complicated manipulations of the string in a simple way, for instance one can withdraw the `'i'` character using the `NGrams` tokenization before detecting `'kenza'` token"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('Qu te Long TEXT w th tabulat ons and new l ne FoR test ng text token zat on', [(0,2),(3,5),(8,12),(13,17),(18,19),(20,22),(23,30),(31,34),(36,39),(40,43),(44,45),(46,48),(49,52),(53,57),(58,60),(61,65),(66,71),(72,75),(76,78)])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "bigrams = ngrams.tokenize(regex='i', ngram=2, inplace=False)\n",
    "bigrams"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[RegexDetector('kenza', [(2,7)])]"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "detections = []\n",
    "for bigram in bigrams:\n",
    "    bigram.subtoksep = ''\n",
    "    kenza = RegexDetector(str(bigram))\n",
    "    kenza.tokenize(regex=\"kenza\")\n",
    "    if kenza:\n",
    "        detections.append(kenza)\n",
    "detections"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unfortunately, the position from the initial string has been lost in the process. To keep it, one in fact needs to clean the string before tokenizing it. This is the realm of [SubstitutionString](https://framagit.org/nlp/substitutionstring) package.\n",
    "\n",
    "Here we can do a patch, that will conserve all the initial characters in the range discovered by the last `RegexDetector`, included the ones withdrawn by the intermediary `NGrams`. To do that, one has to records the number of shift due to a split of the `NGrams` has been done."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[RegexDetector('keniza', [(68,74)])]"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "detections = []\n",
    "for bigram in bigrams:\n",
    "    bigram.subtoksep = ''\n",
    "    kenza = RegexDetector(str(bigram))\n",
    "    kenza.tokenize(regex=\"kenza\")\n",
    "    if kenza:\n",
    "        start = bigram.start + kenza.start\n",
    "        shift = [i for i, r in enumerate(bigram.ranges) \n",
    "                 if bigram.start+kenza.stop in r]\n",
    "        # ranges are not overlapping, so the last char\n",
    "        # in kenza is necessarilly in one and only one range\n",
    "        # of bigram.ranges, hence shift has length 1\n",
    "        stop = bigram.start + kenza.stop + shift[0]\n",
    "        # shift[0] should be multiplied by the length\n",
    "        # of the subtoksep, which is advised to get at 1 \n",
    "        detection = RegexDetector(string=bigram.string,\n",
    "                                  ranges=[range(start, stop),])\n",
    "        detections.append(detection)\n",
    "detections"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
