{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ContextDetector\n",
    "\n",
    "`ContextDetector` is a more elaborated tokenizer than the other ones in this package. It first cuts a string into sub-part, called context, and then it detects a pattern inside each context. The cutting algorithm is instanciated as a `NGrams`, whereas the detection inside each context is performed using the `RegexDetector` algorithm. In addition to this basic cutting-then-detecting usage, `ContextDetector` also attach the left and right context-token to the detected part, where this sub-context part correspond to the part of the string in between two consecutive detections (or the left and/or right edge of the context string if it's the first and/or last detection inside a context, respectively). \n",
    "\n",
    "`ContextDetector` has all the parameters and attributes of a `Span` object (from [tokenspan](https://nlp.frama.io/tokenspan/) package), namely \n",
    " \n",
    " - a `string` that will store the complete text that one wants to tokenize\n",
    " - a list of `intervals`, that selects the sub-string of `string` onto which the tokenization will apply\n",
    " - a `subtoksep` character (preferably of length 1) that glues the sub-tokens (each sub-token being given by a given range in `ranges`) together. By default this is the white space character.\n",
    "\n",
    "Most of the time, one just needs to feed the `string` parameter with the text one wants to tokenize."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ContextDetector.tokenize()\n",
    "\n",
    "The main method of all the tokenizers is `tokenize`. For `ContextDetector` this method has several parameters : \n",
    "\n",
    " - `splitter` (default is `'\\n+'`) is the REGEX that characterizes the patterns that will be transmitted to `NGrams`. By default the conserved tokens are any contiguous chain of characters separated by a new line character.\n",
    " - `splitter_flags` (default is `0`) any flag that are compatible with the [re module], and that will be transmitted to the `NGrams` tokenizer, see (https://docs.python.org/3/library/re.html) from Python Standard Library. To construct the flag, one needs to import the regex module.\n",
    " - `selector` (default is `'\\d+'`) is the REGEX that characterizes the patterns that will be transmitted to `RegexDetector` when it applies to the context string. By default the detected tokens are any contiguous chain of digits.\n",
    " - `selector_flags`  (default is `0`) any flag that are compatible with the [re module], and that will be transmitted to the `RegexDetector` tokenizer.\n",
    "  - `inplace` (default is `True`), a boolean that precise whether the tokenization applies to the current instance, or generate a new instance of the tokenization class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n",
      "3\n",
      "4\n",
      "5\n"
     ]
    }
   ],
   "source": [
    "from iamtokenizing import ContextDetector\n",
    "\n",
    "string = \"test of 0 string\\ntest 1 and 2 then 3 STRING\\n\"\n",
    "string += \"test without digit\\n4 beginning token\\nending token 5\"\n",
    "\n",
    "contexdet = ContextDetector(string=string)\n",
    "contexdet.tokenize()\n",
    "\n",
    "for token in contexdet:\n",
    "    print(token)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Context attributes\n",
    "\n",
    "Each token extracted from the main string now has several attributes that replace them into the context: \n",
    " \n",
    " - `ContextDetector.context` is the list of `NGrams` corresponding to the splits\n",
    " - `ContextDetector.context_parent` is the index of the token inside the `ContextDetector.context` list this detection belongs to\n",
    " - `ContextDetector.context_left` and `ContextDetector.context_right` are the left and right `NGrams` around the detection when the detection is withdrawn from the context splits. Note that, in case of several detections inside a context, the left and right contexts span up to the next detection (or the edge of the string in case it's the first or last detection ; in that case the laft or right context is empty). \n",
    " \n",
    " Let us first see the `.context`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('test of 0 string test 1 and 2 then 3 STRING test without digit 4 beginning token ending token 5', [(0,16),(17,43),(44,62),(63,80),(81,95)])"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "contexdet.context"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a `NGrams` (hence one can loop over this instance if one want to do finer jobs) that corresponds to the different sentences from the parent string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0 test of 0 string\n",
      "1 test 1 and 2 then 3 STRING\n",
      "2 test without digit\n",
      "3 4 beginning token\n",
      "4 ending token 5\n"
     ]
    }
   ],
   "source": [
    "for i, sentence in enumerate(contexdet.context):\n",
    "    print(i, sentence)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each of these sentences still contain their attached detections. The detections are attached through the `context_parent` index: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "detection 0 from sentence n° 0\n",
      "detection 1 from sentence n° 1\n",
      "detection 2 from sentence n° 1\n",
      "detection 3 from sentence n° 1\n",
      "detection 4 from sentence n° 3\n",
      "detection 5 from sentence n° 4\n"
     ]
    }
   ],
   "source": [
    "for det in contexdet:\n",
    "    print(\"detection \" + str(det) + \" from sentence n° \" + str(det.context_parent))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition, one attaches the `context_left` and `context_right` attributes, as the remaining part of the context once the detection is withdrawn, see in particular how the inter-string between the difference detections inside the sentence n°1 are reported from right to left contexts : "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "detection 0 with left context\t'test of '\tand right context\t' string'\n",
      "detection 1 with left context\t'test '\tand right context\t' and '\n",
      "detection 2 with left context\t' and '\tand right context\t' then '\n",
      "detection 3 with left context\t' then '\tand right context\t' STRING'\n",
      "detection 4 with left context\t''\tand right context\t' beginning token'\n",
      "detection 5 with left context\t'ending token '\tand right context\t''\n"
     ]
    }
   ],
   "source": [
    "for det in contexdet:\n",
    "    s = \"detection \"+str(det)+\" with left context\\t'\"\n",
    "    s += str(det.context_left)+\"'\\tand right context\\t'\"\n",
    "    s += str(det.context_right)+\"'\"\n",
    "    print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each of the left and right context are `NGrams`, too, and are therefore manipulable separately."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('test of ', [(0,8)])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "contexdet[0].context_left"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, note that all objects are associated to the complete string: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "contexdet[0].context_left.string == contexdet.string"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "contexdet.context.string == contexdet.string"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Change the REGEX\n",
    "\n",
    "It is possible to change the REGEX associated to both the `NGrams` splitting and `RegexDetection` detection. It is even possible to change them independently, for complicated tasks. We do not describe this possibility here, since it is described in the `NGrams` and `RegexDetector` user guides."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## REGEX flags\n",
    "\n",
    "It is possible to change the flags associated to both the `NGrams` splitting and `RegexDetection` detection. It is even possible to change them independently, for complicated tasks. We do not describe this possibility here, since it is described in the `NGrams` and `RegexDetector` user guides."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transformation to `Span`, `Token` or `string`\n",
    "\n",
    "One can transform the tokens in `Span`, `Token` or string objects easilly from a `ContextDetector` instance. This is nevertheless not the desired behavior, since we will loose the context attributes described above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "spans = contexdet.to_spans()\n",
    "tokens = contexdet.to_tokens()\n",
    "strings = contexdet.to_strings()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Span('5', [(94,95)])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "spans[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "hasattr(spans[-1], 'context')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Token('5', [(94,95)])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tokens[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "hasattr(tokens[-1], 'context')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'5'"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "strings[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "hasattr(strings[-1], 'context')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
