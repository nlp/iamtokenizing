{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# NGrams\n",
    "\n",
    "`NGrams` cuts a string using a REGular Expression (REGEX) to discard some pattern from a parent string. Once the patterns are discarded, what remains from the parent string are called *1-grams*. When there are *n* *1-grams* that are joined together, the resulting tokens are called a *n-grams*.\n",
    "\n",
    "`NGrams` has all the parameters and attributes of a `Span` object (from [tokenspan](https://nlp.frama.io/tokenspan/) package), namely \n",
    " \n",
    " - a `string` that will store the complete text that one wants to tokenize\n",
    " - a list of `intervals`, that selects the sub-string of `string` onto which the tokenization will apply\n",
    " - a `subtoksep` character (preferably of length 1) that glues the sub-tokens (each sub-token being given by a given range in `ranges`) together. By default this is the white space character.\n",
    "\n",
    "Most of the time, one just needs to feed the `string` parameter with the text one wants to tokenize."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## NGrams.tokenize()\n",
    "\n",
    "The main method of all the tokenizers is `tokenize`. For `NGrams` this method has four parameters : \n",
    " \n",
    " - `ngram` (default is `1`) is the number of splitted sub-parts combined into a token\n",
    " - `regex` (default is `'\\s+'`) is the REGEX that characterizes the patterns that serve as spliting flags for the tokenization. By default the splitting takes place on any space (including tabulations, new lines, ...)\n",
    " - `flags` (default is `0`) any flag that are compatible with the [re module](https://docs.python.org/3/library/re.html) from Python Standard Library. To construct the flag, one needs to import the regex module. Examples will be given in the user guide of the class `RegexDetector`.\n",
    "  - `inplace` (default is `True`), a boolean that precise whether the tokenization applies to the current instance, or generate a new instance of the tokenization class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Quite\n",
      "a\n",
      "Long\n",
      "TEXT\n",
      "with\n",
      "tabulations\n",
      "and\n",
      "new\n",
      "line\n",
      "FoR\n",
      "testing\n",
      "text\n",
      "tokenization.\n"
     ]
    }
   ],
   "source": [
    "from iamtokenizing import NGrams\n",
    "\n",
    "string = \"Quite a Long TEXT with tabulations\\t and new line\\n\"\n",
    "string += \"FoR testing text tokenization.\"\n",
    "\n",
    "ngrams = NGrams(string=string)\n",
    "ngrams.tokenize(ngram=1)\n",
    "\n",
    "for token in ngrams:\n",
    "    print(token)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's it, nothing more complicated. \n",
    "\n",
    "## Under the hood\n",
    "\n",
    "Under the hood, the tokenizer fills a list of token, each token being a sub-class of `Span`. Since the `Span` is just a collection of ranges on top of a parent string, each range representing a sub-string of the `Span.string`.\n",
    "\n",
    "For `NGrams` the sub-class of `Span` are `NGrams` objects themselves, and they are stored in the list `NGrams.tokens`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[NGrams('Quite', [(0,5)]),\n",
       " NGrams('a', [(6,7)]),\n",
       " NGrams('Long', [(8,12)]),\n",
       " NGrams('TEXT', [(13,17)]),\n",
       " NGrams('with', [(18,22)]),\n",
       " NGrams('tabulations', [(23,34)]),\n",
       " NGrams('and', [(36,39)]),\n",
       " NGrams('new', [(40,43)]),\n",
       " NGrams('line', [(44,48)]),\n",
       " NGrams('FoR', [(49,52)]),\n",
       " NGrams('testing', [(53,60)]),\n",
       " NGrams('text', [(61,65)]),\n",
       " NGrams('tokenization.', [(66,79)])]"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ngrams.tokens"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's check what the `ngram` does."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Quite a Long\n",
      "a Long TEXT\n",
      "Long TEXT with\n",
      "TEXT with tabulations\n",
      "with tabulations and\n",
      "tabulations and new\n",
      "and new line\n",
      "new line FoR\n",
      "line FoR testing\n",
      "FoR testing text\n",
      "testing text tokenization.\n"
     ]
    }
   ],
   "source": [
    "ngrams.tokenize(ngram=3)\n",
    "\n",
    "for token in ngrams:\n",
    "    print(token)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the reconstructed string is done using the `subtoksep` separator between sub-tokens of a `Span`. This can be checked in several ways : \n",
    " - either by verifying that there are several ranges associated to a `NGrams` when `ngram>1` in `NGrams.tokenize()`\n",
    " - either by changing the `subtoksep` when instanciating `NGrams`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('testing text tokenization.', [(53,60),(61,65),(66,79)])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ngrams[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('testing#text#tokenization.', [(53,60),(61,65),(66,79)])"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ngrams_ = NGrams(string=string, subtoksep='#')\n",
    "ngrams_.tokenize(ngram=3)[-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Change the REGEX\n",
    "\n",
    "By default, the REGEX will split on white space. One can specify the REGEX as we wish. For instance, to split on sentence (defined as two string separated by a new-line character `'\\n'` is easy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Quite a Long TEXT with tabulations\t and new line\n",
      "FoR testing text tokenization.\n"
     ]
    }
   ],
   "source": [
    "sentences = NGrams(string=string)\n",
    "sentences.tokenize(regex='\\n')\n",
    "for sentence in sentences: \n",
    "    print(sentence)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can do anything one wants, as splitting on uppercase characters of spaces for instance. Just recall that the splitting symbol disapears from the tokens."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "uite\n",
      "a\n",
      "ong\n",
      "with\n",
      "tabulations\n",
      "and\n",
      "new\n",
      "line\n",
      "o\n",
      "testing\n",
      "text\n",
      "tokenization.\n"
     ]
    }
   ],
   "source": [
    "uppersplits = NGrams(string=string)\n",
    "uppersplits.tokenize(regex='[A-Z]|\\s+')\n",
    "for upper in uppersplits:\n",
    "    print(upper)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inplace tokenization\n",
    "\n",
    "By default, the tokenization is performed inplace, that is, the object changes after a tokenization, which allows to make quite fine construction of tokenizer, since the next `tokenize` method will be applied to the kept tokens from the previous tokenization. Below is an example where one splits around the uppercase letters once the sentence tokenization is done."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "uite a \n",
      "ong \n",
      " with tabulations\t and new line\n",
      "o\n",
      " testing text tokenization.\n"
     ]
    }
   ],
   "source": [
    "sentences.tokenize(regex=\"[A-Z]\")\n",
    "for sentence in sentences:\n",
    "    print(sentence)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nevertheless, one might be interested by saving a step of the tokenization process, in which case one can use `inplace=False` parameter of the `tokenize` method ; default is `inplace=True`. Let redo the previous two-steps tokenization, and save the first one for later use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Quite a Long TEXT with tabulations\t and new line\n",
      "FoR testing text tokenization.\n"
     ]
    }
   ],
   "source": [
    "sentences = NGrams(string=string)\n",
    "saved_sentences = sentences.tokenize(regex='\\n', inplace=False)\n",
    "for sentence in saved_sentences: \n",
    "    print(sentence)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `sentences` instance if still untouched by the `inplace=False` process. So applying the second tokenization (splitting arounf uppercase letters) will not produce the same result, obviously"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "NGrams('Quite a Long TEXT with tabulations\t and new line\n",
       "FoR testing text tokenization.', [(0,79)])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sentences"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "uite a \n",
      "ong \n",
      " with tabulations\t and new line\n",
      "\n",
      "o\n",
      " testing text tokenization.\n"
     ]
    }
   ],
   "source": [
    "sentences.tokenize(regex=\"[A-Z]\")\n",
    "for sentence in sentences:\n",
    "    print(sentence)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transformation to `Span`, `Token` or `string`\n",
    "\n",
    "One can transform the tokens in `Span`, `Token` or string objects easilly from a `NGrams` instance. This allows easy manipulation of the token in further NLP treatments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "spans = ngrams.to_spans()\n",
    "tokens = ngrams.to_tokens()\n",
    "strings = ngrams.to_strings()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Span('testing text tokenization.', [(53,60),(61,65),(66,79)])"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "spans[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Token('testing text tokenization.', [(53,60),(61,65),(66,79)])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tokens[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'testing text tokenization.'"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "strings[-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## NGrams to RegexDetector and vice-versa\n",
    "\n",
    "One can give the tokens to the kind-of opposite-in-task tokenizer quite easilly. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "RegexDetector('Quite a Long TEXT with tabulations and new line FoR testing text tokenization.', [(0,5),(6,7),(8,12),(13,17),(18,22),(23,34),(36,39),(40,43),(44,48),(49,52),(53,60),(61,65),(66,79)])"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from iamtokenizing import RegexDetector\n",
    "\n",
    "regdet = RegexDetector(ngrams)\n",
    "regdet"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then one can detect inside the cleaned string, by using a `RegexDetector` after a `NGrams` cleaner. Below is an idiomatic example, since it would have given the same thing without the passage by `NGrams`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "RegexDetector('tab', [(23,26)])"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "regdet.tokenize(regex=\"tab\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
