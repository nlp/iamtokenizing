# iamtokenizing

Those are the elaborated tokenizers based on the `Span` object (see [tokenspan](https://framagit.org/nlp/tokenspan) package). They acts using their `.tokenize()` method.

## `BaseTokenizer`

This class share common methods among the other tokenizers. In particular, it deals with the possibility to export the tokens in instances of `Span` or `Tokens` or sub-strings. 

It is not a tokenizer at all.

## `CharGrams` 

Cuts a string into contiguous and overlapping ranges of characters.

```python
from iamtokenizing import CharGrams
s = "ABCDEF"
CharGrams(s).tokenize().to_strings()
# -> ['A', 'B', 'C', 'D', 'E', 'F']
CharGrams(s).tokenize(size=2).to_strings()
# -> ['AB', 'BC', 'CD', 'DE', 'EF']
CharGrams(s).tokenize(size=2,step=2).to_strings()
# -> ['AB', 'CD', 'EF']
CharGrams(s,ranges=[range(1,3),range(4,6)],subtoksep='@').tokenize(size=2).to_tokens()
# -> Tokens(4 Token) : BC  C@  @E  EF # a Tokens object
```

## `NGrams` and `RegexDetector`

`NGrams` cuts a string based on some REGular EXpression (REGEX). The case by default is to cut the initial string on its white space for instance. 

`RegexDetector` extracts the tokens that correspond to a REGular EXpression (REGEX).

The difference with `RegexDetector` is that `NGrams` keeps the string in between the patterns found from the REGEX, whereas `RegexDetector` keeps the REGEX patterns themselves. So for instance defining

```python
from iamtokenizing import NGrams, RegexDetector
string = "This is\ta string"
ngrams = NGrams(string,subtoksep='_').tokenize()
regdet = RegexDetector(string,subtoksep='_').tokenize(regex='\s+')
```

will end up with complementary ranges

```python
# ngrams -> NGrams('This_is_a_string', [(0,4),(5,7),(8,9),(10,16)])
# regdet -> RegexDetector(' _   _ ', [(4,5),(7,8),(9,10)])
```

Note also the difference : 
 - `NGrams` takes the REGEX as initialization parameter
 - `RegexDetector` takes the REGEX as parameter in the `.tokenize` method